# Call Billing

This is a simple program that calculates the total amount of money a user has to pay based on the total number of call blocks used. Each call is divided into blocks of 30 seconds, with any remaining seconds counted as an additional block.

# Requirements
1. Python 3.8+
2. Docker
3. Docker Compose


# Getting Started
## 1. Clone the repository:


``
git clone https://github.com/your-username/call-billing.git
``

## 2. Create a .env file in the root directory with the following environment variables:
```commandline
DB_HOST=db
DB_PORT=5432
DB_NAME=calling
DB_USER=calling_user
DB_PASSWORD=mypassword
SANIC_PORT=8000
```


## Start the database and the application by running:
``
docker-compose up
``


## Access the application at http://localhost:8000

## To stop the application and the database, run:
``
docker-compose down
``