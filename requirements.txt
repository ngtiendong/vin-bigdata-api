sanic==23.3.0
python-decouple==3.8
asyncpg==0.27.0
sanic-testing==23.3.0
pytest==7.2.2
tortoise-orm==0.19.3
