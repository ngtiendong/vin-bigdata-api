from sanic import Sanic
from sanic.request import Request
from sanic.response import json
from typing import Dict
import traceback
from utils import const
from tortoise import run_async
from utils.database.initialize import init_db
from utils.database.models import User, CallBilling
from tortoise.expressions import Q
from tortoise import transactions
from decouple import config

app = Sanic(__name__)

# Chạy thiết lập kết nối đến database và tạo bảng
run_async(init_db())


async def get_user_data(username):
    """
    return user data in model
    :param username: user name from request
    :return: user object
    """
    # Get User information
    user = await User.filter(Q(username=username)).get_or_none()
    if user is None:
        # Create user
        user = User(username=username)
        await user.save()
    return user


@app.put('/mobile/<username>/call')
async def api_record_call(request: Request, username: str):
    """
    Calculate block_count base on call duration
    :param request: request object of Sanic
    :param username: username to save into dictionary
    :return: json sanic response
    """
    try:
        # Get User information
        async with transactions.in_transaction():
            user = await get_user_data(username)

            # Create calling record
            call_duration = int(request.form.get('call_duration'))
            block_count = int((call_duration / 1000) // 30) + 1  # round up to next block
            call_billing = CallBilling(user=user, call_duration=call_duration, block_call=block_count)
            await call_billing.save()

            return json(
                {'status': const.STATUS_SUCCESS, 'message': f'Call recorded for user {username} with {block_count} blocks'})
    except (ValueError, TypeError, KeyError) as e:
        traceback.print_exc()
        return json({'status': const.STATUS_FAIL, 'error': 'Invalid request data, ' + str(e)}, status=400)


@app.get('/mobile/<username>/billing')
async def api_get_billing(request: Request, username: str):
    """
    API to calculate billing base on block count of each username
    :param request: Request object of
    :param username: username to get data from dictionary/DB
    :return: json sani response
    """
    try:
        # Get User information
        user = await User.filter(Q(username=username)).get_or_none()
        if user is not None:
            calling_query = CallBilling.filter(user=user)
            list_call_duration = await calling_query.values_list('call_duration', flat=True)
            list_block_count = await calling_query.values_list('block_call', flat=True)

            return json({'status': const.STATUS_SUCCESS,
                         'call_count': int(sum(x for x in list_call_duration if isinstance(x, int))/1000),
                         'unit_call_count': 'second',
                         'block_count': sum(y for y in list_block_count if isinstance(y, int))})
        else:
            # Not found user
            return json({'status': const.STATUS_FAIL, 'message': f"Not found user with username: {username}"})
    except Exception as e:
        traceback.print_exc()
        return json({"status": const.STATUS_FAIL, 'message': 'Invalid request data, ' + str(e)}, status=400)


if __name__ == '__main__':
    app.run(
        host='0.0.0.0',
        debug=True,
        port=int(config('SANIC_PORT'))
    )
