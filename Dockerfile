# Sử dụng một image có sẵn Python 3.9
FROM python:3.8.10

WORKDIR /app

COPY requirements.txt .

RUN pip install -r requirements.txt

COPY . /app

# Đặt biến môi trường cho ứng dụng
ENV SANIC_PORT 8000

# Chạy ứng dụng
CMD ["python", "app.py"]