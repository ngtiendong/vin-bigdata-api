from tortoise import Model, fields


class User(Model):
    id = fields.UUIDField(pk=True, index=True)
    username = fields.CharField(max_length=255, unique=True)
    created = fields.DatetimeField(auto_now_add=True)
    updated = fields.DatetimeField(auto_now=True, null=True)

    class Meta:
        table = "user"

    def __str__(self):
        return f"User name: {self.username}"


class CallBilling(Model):
    id = fields.UUIDField(pk=True, index=True)
    user = fields.ForeignKeyField('models.User')
    call_duration = fields.IntField()
    block_call = fields.IntField()
    created = fields.DatetimeField(auto_now_add=True)
    updated = fields.DatetimeField(auto_now=True, null=True)

    class Meta:
        table = "call_billing"

    def __str__(self):
        return f"Call billing id: {self.id}"
