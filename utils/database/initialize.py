from tortoise import Tortoise, fields, run_async
from decouple import config

# cấu hình thông tin kết nối đến PostgreSQL

POSTGRES_HOST = config('DB_HOST')
POSTGRES_PORT = config('DB_PORT')
POSTGRES_USER = config('DB_USER')
POSTGRES_PASSWORD = config('DB_PASSWORD')
POSTGRES_DB = config('DB_NAME')


# Thiết lập kết nối đến PostgreSQL và tạo bảng nếu chưa tồn tại
async def init_db():
    await Tortoise.init(
        db_url=f"postgres://{POSTGRES_USER}:{POSTGRES_PASSWORD}@{POSTGRES_HOST}:{POSTGRES_PORT}/{POSTGRES_DB}",
        modules={"models": ["utils.database.models"]},
    )
    await Tortoise.generate_schemas()

