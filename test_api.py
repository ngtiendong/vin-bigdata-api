import uuid
from app import app
from sanic_testing.testing import SanicTestClient, SanicASGITestClient
import random
from utils import const
import json
import asyncio


async def run_double_test():
    async with SanicASGITestClient(app) as test_client:
        for i in range(10):
            username = str(uuid.uuid4()) + "_ntdong_test"
            data_body = {"call_duration": random.randint(10000, 100000)}
            request, response = await test_client.put(f"mobile/{username}/call", data=data_body)
            resp_json = json.loads(response.body)

            assert request.method.lower() == "put"
            assert response.status == 200
            assert resp_json['status'] == const.STATUS_SUCCESS, "Something wrong in call API"

            request, response = await test_client.get(f"mobile/{username}/billing")
            resp_json = json.loads(response.body)
            print("Billing: ", resp_json)
            assert request.method.lower() == "get"
            assert response.status == 200
            assert resp_json['status'] == const.STATUS_SUCCESS, "Something wrong in Billing API"


if __name__ == "__main__":
    asyncio.run(run_double_test())
